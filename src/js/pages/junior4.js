import * as THREE from 'three'
import Scene3D from "../scene3d";

/** Создать сцену с 2 кубами, при клике на любой кубу сделать все стороны второго куба равными расстоянию от точки клика до центра геометрии куба по которому кликнули. */
class Junior4 extends Scene3D {

  #cube1

  #cube2

  constructor() {

    super();

    const cubeGeometry = new THREE.BoxBufferGeometry(3, 3, 3);
    const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0x00aa00 });

    this.#cube1 = new THREE.Mesh(cubeGeometry, cubeMaterial);
    this.#cube2 = new THREE.Mesh(cubeGeometry, cubeMaterial);

    this.#cube1.position.x = -4;
    this.#cube2.position.x = 4;

    this.scene.add(this.#cube1);
    this.scene.add(this.#cube2);

  }    

  onClick(e) {
    this.raycaster.setFromCamera(e.pointer, this.camera);
    const objects = [ this.#cube1, this.#cube2 ];
    const intersects = this.raycaster.intersectObjects(objects, true);
    if (!intersects.length) return;
    objects.forEach((cube, index) => {
      if (intersects[0].object != cube) return;
      const d = intersects[0].point.distanceTo(cube.position);
      const cube2 = objects[1 - index];
      cube2.geometry = new THREE.BoxBufferGeometry(d, d, d);
    })
  }


}

const app = new Junior4();