import * as THREE from 'three'
import Scene3D from "../scene3d";

/** Создать сцену с 2 кубами, при клике на любой кубу добавить на сцену линию пересекающую куб в точке клика. */
class Junior2 extends Scene3D {

  #cube1

  #cube2

  #lineMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff })
  
  constructor() {

    super();

    const cubeGeometry = new THREE.BoxBufferGeometry(3, 3, 3);
    const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0x00aa00 });

    this.#cube1 = new THREE.Mesh(cubeGeometry, cubeMaterial);
    this.#cube2 = new THREE.Mesh(cubeGeometry, cubeMaterial);

    this.#cube1.position.x = -4;
    this.#cube2.position.x = 4;

    this.scene.add(this.#cube1);
    this.scene.add(this.#cube2);

  }    

  onClick(e) {
    this.raycaster.setFromCamera(e.pointer, this.camera);
    const objects = [ this.#cube1, this.#cube2 ];
    const intersects = this.raycaster.intersectObjects(objects, true);
    if (!intersects.length) return;
    objects.forEach(cube => {
      if (intersects[0].object != cube) return;
      const normal = intersects[0].face.normal;
      const point = intersects[0].point;
      const points = [ 
        new THREE.Vector3(point.x + normal.x, point.y + normal.y, point.z + normal.z), 
        point 
      ];
      const lineGeometry = new THREE.BufferGeometry().setFromPoints(points);
      const line = new THREE.Line(lineGeometry, this.#lineMaterial);
      this.scene.add(line);
    })
  }


}

const app = new Junior2();