import * as THREE from 'three'
import Scene3D from "../scene3d";

/** Создать сцену с 3 кубами, так чтобы второй был ребёнком первого а третий ребёнком второго. При клике по кубам рандомно менять их позицию и пересчитывать расстояние между всеми кубами до камеры. */
class Junior5 extends Scene3D {

  #cube1

  #cube2

  #cube3
  
  constructor() {

    super();

    const cubeGeometry = new THREE.BoxBufferGeometry(1.5, 1.5, 1.5);
    const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0x00aa00 });

    this.#cube1 = new THREE.Mesh(cubeGeometry, cubeMaterial);
    this.#cube2 = new THREE.Mesh(cubeGeometry, cubeMaterial);
    this.#cube3 = new THREE.Mesh(cubeGeometry, cubeMaterial);

    this.scene.add(this.#cube1);
    this.#cube1.add(this.#cube2);
    this.#cube2.add(this.#cube3);

    this.#cube1.position.x = -3;
    this.#cube2.position.x = 2;
    this.#cube3.position.x = 2;

  }    

  onClick(e) {
    this.raycaster.setFromCamera(e.pointer, this.camera);
    const objects = [ this.#cube1, this.#cube2, this.#cube3 ];
    const worldPositions = objects.map(cube => cube.localToWorld(new THREE.Vector3(0, 0, 0))); 
    const intersects = this.raycaster.intersectObjects(objects, true);
    if (!intersects.length) return;
    objects.forEach((cube, index) => {
      if (intersects[0].object != cube) return;
      cube.position.add(new THREE.Vector3(0.3*(0.5 - Math.random()), 0.3*(0.5 - Math.random()), 0.3*(0.5 - Math.random())));
      cube.updateWorldMatrix();
      for (let i = index + 1; i < objects.length; i++) {
        const p = objects[i - 1].worldToLocal(worldPositions[i].clone());
        objects[i].position.set(p.x, p.y, p.z);  
        objects[i].updateWorldMatrix();
      }
    })
  }


}

const app = new Junior5();