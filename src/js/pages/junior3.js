import * as THREE from 'three'
import Scene3D from "../scene3d";

/** Создать сцену с 2 кубами, при клике на любой кубу добавить на сцену линию пересекающую куб в точке клика, центры геометрий кубов. */
class Junior3 extends Scene3D {

  #cube1

  #cube2

  /** Отображаемая сетка 1-го куба */
  #edges1

  /** Отображаемая сетка 2-го куба */
  #edges2

  #lineMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff })
  
  constructor() {

    super();

    const cubeGeometry = new THREE.BoxBufferGeometry(3, 3, 3);
    const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0x00aa00 });

    this.#cube1 = new THREE.Mesh(cubeGeometry, cubeMaterial);
    this.#cube2 = new THREE.Mesh(cubeGeometry, cubeMaterial);

    this.#cube1.position.x = -4;
    this.#cube2.position.x = 4;

    this.#cube1.visible = false;
    this.#cube2.visible = false;

    this.scene.add(this.#cube1);
    this.scene.add(this.#cube2);
    
    const edgesGeometry = new THREE.EdgesGeometry(cubeGeometry);

    this.#edges1 = new THREE.LineSegments(edgesGeometry, this.#lineMaterial);
    this.#edges2 = new THREE.LineSegments(edgesGeometry, this.#lineMaterial);
    this.#edges1.matrix = this.#cube1.matrix;
    this.#edges2.matrix = this.#cube2.matrix;
    this.#edges1.position.x = this.#cube1.position.x;
    this.#edges2.position.x = this.#cube2.position.x;

    this.scene.add(this.#edges1);
    this.scene.add(this.#edges2);

  }    

  onClick(e) {
    this.raycaster.setFromCamera(e.pointer, this.camera);
    const objects = [ this.#cube1, this.#cube2 ];
    const intersects = this.raycaster.intersectObjects(objects, true);
    if (!intersects.length) return;
    objects.forEach(cube => {
      if (intersects[0].object != cube) return;
      const points = [ intersects[0].point, cube.position ];
      const lineGeometry = new THREE.BufferGeometry().setFromPoints(points);
      const line = new THREE.Line(lineGeometry, this.#lineMaterial);
      this.scene.add(line);
    })
  }


}

const app = new Junior3();