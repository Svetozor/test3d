import * as THREE from 'three'
import Scene3D from "../scene3d";

/** Создать сцену с 2 кубами, при клике на любой кубу рандомно менять ему размер, поворот, цвет материала. */
class Junior1 extends Scene3D {

  #cube1

  #cube2
  
  constructor() {

    super();

    const cubeGeometry = new THREE.BoxBufferGeometry(3, 3, 3);

    this.#cube1 = new THREE.Mesh(cubeGeometry, new THREE.MeshBasicMaterial({ color: 0x0000aa }));
    this.#cube2 = new THREE.Mesh(cubeGeometry, new THREE.MeshBasicMaterial({ color: 0x00aa00 }));

    this.#cube1.position.x = -4;
    this.#cube2.position.x = 4;
    
    this.scene.add(this.#cube1);
    this.scene.add(this.#cube2);

  }    

  onClick(e) {
    this.raycaster.setFromCamera(e.pointer, this.camera);
    const objects = [ this.#cube1, this.#cube2 ];
    const intersects = this.raycaster.intersectObjects(objects, true);
    if (!intersects.length) return;
    objects.forEach(cube => {
      if (intersects[0].object != cube) return;
      const s = 0.5 + 0.5*Math.random();
      const a = Math.PI*Math.random();
      cube.scale.set(s, s, s);
      cube.rotation.set(a, a, a);
      cube.material.color = new THREE.Color(Math.round(0x999999*Math.random()))
    })
  }


}

const app = new Junior1();