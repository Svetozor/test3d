import { DiscreteInterpolant } from 'three';
import pages from '../pages.js';

const div = document.getElementById('page');
div.style.padding = '20px';

pages.forEach(page => {
  if (page.id == 'index') return;
  div.innerHTML += `<a href="${page.url}">${page.title}</a> <br/>`;
})



