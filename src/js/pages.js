
/** Массив страниц сайта 
 * id - Уникальный идентификатор (лучше не менять)
 * url - Адрес страницы
 * title - Заголовок страницы
*/
const pages = [
  { id: 'index', url: 'index.html', title: "Тест 3D" },
  { id: 'junior1', url: 'junior1.html', title: "Junior 1" },
  { id: 'junior2', url: 'junior2.html', title: "Junior 2" },
  { id: 'junior3', url: 'junior3.html', title: "Junior 3" },
  { id: 'junior4', url: 'junior4.html', title: "Junior 4" },
  { id: 'junior5', url: 'junior5.html', title: "Junior 5" },
]  

module.exports = pages; 