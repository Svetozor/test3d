import * as THREE from 'three'
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls.js'

class Scene3D {

  #camera = new THREE.PerspectiveCamera(75, 1, 0.1, 1000);

  #renderer

  #raycaster = new THREE.Raycaster()

  #scene = new THREE.Scene()

  #div = document.getElementById('page')

  #controls

  #onResize

  #onFrame

  #onMouseDown

  #onMouseUp  

  constructor() {

    const canvas = document.createElement('canvas');
    canvas.style.width = canvas.style.height = '100%';
    this.#div.appendChild(canvas);

    this.#camera.position.z = 10;

    this.#renderer = new THREE.WebGLRenderer({ canvas: canvas, context: canvas.getContext('webgl') });
    this.#onResize = () => {
      const w = this.#div.clientWidth, h = this.#div.clientHeight;
      this.#camera.aspect = w/h;
      this.#camera.lookAt(new THREE.Vector3(0, 0, 0));
      this.#camera.updateProjectionMatrix();
      if (this.#renderer) this.#renderer.setSize(w, h);
    }
    window.addEventListener('resize', this.#onResize)
    this.#onResize();

    this.#onFrame = () => {
      if (this.#controls) this.#controls.update();
      this.#renderer.render(this.#scene, this.#camera);
      requestAnimationFrame(this.#onFrame);
    }
    this.#onFrame();

    let mouseDownTime = 0;
    this.#onMouseDown = event => {
      const e = event.changedTouches ? event.changedTouches[0] : event;
      if ((e.type == 'mousedown') && (e.button != 0)) return mouseDownTime = 0; 
      mouseDownTime = Date.now();
    }
    this.#onMouseUp = event => {
      const e = event.changedTouches ? event.changedTouches[0] : event;
      if ((e.type == 'mousedown') && (e.button != 0)) return mouseDownTime = 0; 
      if (Date.now() - mouseDownTime > 500) return mouseDownTime = 0; 
      e.pointer = new THREE.Vector2();
      e.pointer.x = (e.clientX / this.canvas.width)*2 - 1;
      e.pointer.y = -(e.clientY / this.canvas.height)*2 + 1;
      this.onClick(e);
    }
    canvas.addEventListener('mousedown', this.#onMouseDown);
    canvas.addEventListener('mouseup', this.#onMouseUp);
    canvas.addEventListener('touchstart', this.#onMouseDown);
    canvas.addEventListener('touchend', this.#onMouseUp);

    this.#controls = new TrackballControls(this.#camera, this.#renderer.domElement);
    this.#controls.minDistance = 1;
    this.#controls.maxDistance = 500;

  }  

  get scene() { return this.#scene }

  get camera() { return this.#camera }

  get raycaster() { return this.#raycaster }

  get canvas() { return this.#renderer ? this.#renderer.domElement : null }

  onClick(e) {}

  destroy() {
    window.removeEventListener('resize', this.#onResize);
    this.canvas.removeEventListener('mousedown', this.#onMouseDown);
    this.canvas.removeEventListener('mouseup', this.#onMouseUp);
    this.canvas.removeEventListener('touchstart', this.#onMouseDown);
    this.canvas.removeEventListener('touchend', this.#onMouseUp);
    this.#onFrame = () => { return };
    this.#div.removeChild(this.canvas);
  }

}

export default Scene3D;