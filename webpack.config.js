const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const pages = require('./src/js/pages.js');

const entries = {};
pages.forEach(page => entries[page.id] = path.resolve(__dirname, "./src/js/pages/" + page.id + ".js"));

module.exports = {
  entry: entries,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: { 
          presets: ["@babel/preset-env"], 
          plugins: ["@babel/plugin-transform-runtime"]
        }
      },
      {
        test: /\.(svg|png|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'img/[hash][ext][query]'
        }
      }
    ],
  },
  resolve: {
    extensions: [".js"],
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "js/[name].js",
    chunkFilename: "js/[name].js",
    clean: true,
  },
  devServer: {
    static: {
      directory: path.resolve(__dirname, "./dist"),
    },
    port: 9000,
  },
  plugins: [
    ...pages.map(page => new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/html/page_layout.html"),
      filename: page.url,
      title: page.title,
      templateParameters: { pageId: page.id },
      inject: false,
      hot: true,
    }))
  ],
};
